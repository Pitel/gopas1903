package cz.gopas.praha1903;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.realm.Realm;

public class ConstantFragment extends Fragment {

	private ConstantListener mListener;
	private static final String TAG = ConstantFragment.class.getSimpleName();
	private Realm realm;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_constant, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		final RecyclerView recycler = (RecyclerView) view;
		recycler.setLayoutManager(new LinearLayoutManager(requireContext()));
		recycler.setHasFixedSize(true);
		recycler.setAdapter(new ConstantAdapter(realm.where(Constant.class).findAll()));
		/*
		for (final Constant c : realm.where(Constant.class).findAll()) {
			Log.v(TAG, c.getName() + " = " + c.getValue());
		}
		*/
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof ConstantListener) {
			mListener = (ConstantListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement ConstantListener");
		}
		realm = Realm.getDefaultInstance();
	}

	@Override
	public void onDetach() {
		realm.close();
		super.onDetach();
		mListener = null;
	}

	public interface ConstantListener {
		void constantSelected(double constant);
	}
}
