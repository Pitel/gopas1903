package cz.gopas.praha1903;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Constant extends RealmObject {
	@PrimaryKey
	private String name;
	private double value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
}
