package cz.gopas.praha1903;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class ConstantAdapter extends RealmRecyclerViewAdapter<Constant, ConstantAdapter.ConstantViewHolder> {

	private static final String TAG = ConstantAdapter.class.getSimpleName();

	public ConstantAdapter(OrderedRealmCollection<Constant> data) {
		super(data, true);
	}

	@NonNull
	@Override
	public ConstantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		Log.d(TAG, "onCreateViewHolder");
		final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		return new ConstantViewHolder(
				inflater.inflate(android.R.layout.simple_list_item_2, parent, false)
		);
	}

	@Override
	public void onBindViewHolder(@NonNull ConstantViewHolder holder, int position) {
		final Constant c = getItem(position);
		//holder.itemView.setOnClickListener();
		holder.name.setText(c.getName());
		holder.value.setText(String.valueOf(c.getValue()));
	}

	public class ConstantViewHolder extends RecyclerView.ViewHolder {
		public TextView name, value;

		public ConstantViewHolder(View row) {
			super(row);
			name = row.findViewById(android.R.id.text1);
			value = row.findViewById(android.R.id.text2);
		}
	}
}
