package cz.gopas.praha1903;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import io.realm.Realm;
import io.realm.RealmConfiguration;

final public class MainActivity
		extends AppCompatActivity
		implements ConstantFragment.ConstantListener {

	private static final String TAG = MainActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		FragmentManager.enableDebugLogging(BuildConfig.DEBUG);
		Realm.init(this);
		Realm.setDefaultConfiguration(
				new RealmConfiguration.Builder()
						.initialData(realm -> {
							Constant c = realm.createObject(Constant.class, "Pi");
							c.setValue(Math.PI);
							c = realm.createObject(Constant.class, "e");
							c.setValue(Math.E);
							c = realm.createObject(Constant.class, "The Answer");
							c.setValue(42);

							for (int i = 0; i < 500; i++) {
								c = realm.createObject(Constant.class, String.valueOf(i));
								c.setValue(i);
							}
						})
						.build()
		);
		getSupportFragmentManager().addOnBackStackChangedListener(this::fragmentBackStackChanged);
		fragmentBackStackChanged();

		Toast.makeText(this, "Create", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "Create");

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.replace(android.R.id.content, new MainFragment())
					.commit();
		}

		try {
			Log.i(TAG, getIntent().getDataString());
		} catch (Throwable t) {
			//Ignore
		}
	}

	private void fragmentBackStackChanged() {
		getSupportActionBar().setDisplayHomeAsUpEnabled(
				getSupportFragmentManager().getBackStackEntryCount() > 0
		);
	}

	@Override
	public boolean onSupportNavigateUp() {
		getSupportFragmentManager().popBackStack();
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "Resume");
	}

	@Override
	protected void onPause() {
		Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "Pause");
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		Log.d(TAG, "Destroy");
		super.onDestroy();
	}

	@Override
	public void constantSelected(double constant) {
		Log.d(TAG, String.valueOf(constant));
	}
}
