package cz.gopas.praha1903;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainFragment extends Fragment {

	private static final String TAG = MainFragment.class.getSimpleName();

	@BindView(R.id.op)
	RadioGroup op;
	@BindView(R.id.a)
	TextView aText;
	@BindView(R.id.b)
	TextView bText;
	@BindView(R.id.res)
	TextView res;

	private Unbinder unbinder;

	private SharedPreferences prefs;

	@Override
	public void onAttach(@NonNull Context context) {
		super.onAttach(context);
		setHasOptionsMenu(true);
		prefs = PreferenceManager.getDefaultSharedPreferences(context);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_main, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		unbinder = ButterKnife.bind(this, view);

		//useless, because freezesText is true
		//res.setText(savedInstanceState.getString("result", ""));

		op.setOnCheckedChangeListener((group, id) -> calc());
	}

	@Override
	public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
		inflater.inflate(R.menu.main, menu);
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		switch (item.getItemId()) {
			case R.id.about:
				getFragmentManager().beginTransaction()
						.replace(android.R.id.content, new AboutFragment())
						.addToBackStack(null)
						.commit();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		((MainActivity) requireActivity()).getSupportActionBar().setTitle(R.string.app_name);
	}

	@OnClick(R.id.calc)
	void calc() {
		final Double a = Double.parseDouble(aText.getText().toString());
		final Double b = Double.parseDouble(bText.getText().toString());
		final Double result;
		switch (op.getCheckedRadioButtonId()) {
			case R.id.add:
				result = a + b;
				break;
			case R.id.sub:
				result = a - b;
				break;
			case R.id.mul:
				result = a * b;
				break;
			case R.id.div:
				result = a / b;
				if (b == 0) {
					new AlertDialog.Builder(requireContext())
							.setTitle(R.string.zero_div)
							.setMessage(R.string.implosion)
							.setPositiveButton(android.R.string.ok, (dialog, which) -> {
								Toast.makeText(requireContext(), "Positive", Toast.LENGTH_SHORT).show();
								dialog.dismiss();
							})
							.setNegativeButton(android.R.string.no, (dialog, which) -> {
								Toast.makeText(requireContext(), "Negative", Toast.LENGTH_SHORT).show();
								dialog.dismiss();
							})
							.setCancelable(false)
							.show();
				}
				break;
			default:
				result = Double.NaN;
				Toast.makeText(requireContext(), "Unknown operation", Toast.LENGTH_SHORT).show();
				Log.w(TAG, "Unknown operation");
		}
		prefs.edit().putFloat("res", result.floatValue()).apply();
		res.setText(String.valueOf(result));
	}

	@OnClick(R.id.ans)
	void ans() {
		bText.setText(String.valueOf(prefs.getFloat("res", 0)));
	}

	@OnClick(R.id.constant)
	void constant() {
		getFragmentManager().beginTransaction()
				.replace(android.R.id.content, new ConstantFragment())
				.addToBackStack(null)
				.commit();
	}

	@OnClick(R.id.share)
	void share() {
		final Intent intent = new Intent(Intent.ACTION_SEND)
				.setType("text/plain")
				.putExtra(Intent.EXTRA_TEXT, res.getText().toString());
		if (intent.resolveActivity(requireContext().getPackageManager()) != null) {
			startActivity(intent);
		} else {
			Toast.makeText(requireContext(), getString(R.string.no_sharing_activity), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);
		//useless, because freezesText is true
		//outState.putString("result", res.getText().toString());
	}

	@Override
	public void onDestroyView() {
		unbinder.unbind();
		unbinder = null;
		super.onDestroyView();
	}
}
